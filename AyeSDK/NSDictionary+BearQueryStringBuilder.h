//
//  NSDictionary+BearQueryStringBuilder.h
//  AyeSDK
//
//  Created by Bart van den Berg on 17/10/2017.
//  Copyright © 2017 BlueGiraffe Games B.V. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (BearQueryStringBuilder)

-(NSString*) urlEncodedString;
@end
