//
//  GiraffeReachability.h
//
//  Created by Bart van den Berg on 20/09/16.
//  Copyright © 2016 BlueGiraffe Games B.V. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    PTNetworkStatusUnreachable = 0,
    PTNetworkStatusReachableWiFi,
    PTNetworkStatusReachableCarrier,
    PTNetworkStatusReachableUnknown,
} GiraffeNetworkStatus;



#define PTReachabilityDidChangeNotification @"PTReachabilityDidChangeNotification"

#define PTNetworkUnreachable                @"unreachable"
#define PTNetworkReachableWiFi              @"wi-fi"
#define PTNetworkReachableCarrier           @"carrier"
#define PTNetworkReachableUnknown           @"unknown"


@interface GiraffeReachability : NSObject

+ (BOOL)startGeneratingNotifications;

// Stops listening for reachability notifications
+ (void)stopGeneratingNotificatons;

+ (GiraffeNetworkStatus)currentReachabilityStatus;
+ (NSString *)currentReachabilityStatusString;

@end
