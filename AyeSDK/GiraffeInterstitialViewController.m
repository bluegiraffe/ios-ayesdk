//
//  GiraffeInterstitialWebViewController.m
//  GiraffeSDK
//
//  Created by Bart van den Berg on 25/01/2017.
//  Copyright © 2017 BlueGiraffe Games B.V. All rights reserved.
//

#import "GiraffeInterstitialViewController.h"

@interface GiraffeInterstitialViewController ()

@end

@implementation GiraffeInterstitialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
}

-(void)webViewDidStartLoad:(UIWebView *)webViewer
{
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webViewer
{
    // Send payload to interface
    NSString *resultJSON = [self parsePayloadToJSON:self.payload];
    if(resultJSON.length>0)
    {
        [webViewer stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"injectVars(%@)",resultJSON]];
    }
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.30];
    webView.alpha = 1;
    [UIView commitAnimations];
    

    
}




- (BOOL)webView:(UIWebView *)webView2 shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSString *requestString = [[request URL] absoluteString];
    //NSLog(@"request : %@",requestString);
    
    if ([requestString hasPrefix:@"giraffe-js:"]) {
        
        NSArray *components = [requestString componentsSeparatedByString:@":"];
        NSString *function = (NSString *)[components objectAtIndex:1];
        [self handleJavascriptCall:function];
        
        return NO;
    }
    
    if ([requestString hasPrefix:@"giraffe-http:"])
    {
        NSArray *components = [requestString componentsSeparatedByString:@"giraffe-http:"];
        NSString *url = (NSString *)[components objectAtIndex:1];
        [self handleExternal:url];

    }
    
    return YES;
}

-(void)handleExternal:(NSString *)url
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    [self dismissViewController:nil];
}

- (void)handleJavascriptCall:(NSString *)function
{
    if(function)
    {
        NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
        NSArray *urlComponents = [function componentsSeparatedByString:@"&"];
        
        for (NSString *keyValuePair in urlComponents)
        {
            NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
            NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
            NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
            
            [queryStringDictionary setObject:value forKey:key];
        }
        
        [self dismissViewController:queryStringDictionary];
        
//        if ([function isEqualToString:@"close"])
//        {
//            [self dismissViewController:queryStringDictionary];
//        }
//        
//        if ([function isEqualToString:@"upsell"])
//        {
//            [self dismissViewController:queryStringDictionary];
//        }
        
    }
   
}


-(void)dismissViewController:(NSMutableDictionary *)payload
{
    id<GiraffeInterstitialViewControllerDelegate> strongDelegate = self.delegate;
    [self dismissViewControllerAnimated:NO completion:^{
        
        if ([strongDelegate respondsToSelector:@selector(didDismissGiraffeInterstitialViewController:)])
        {
            [strongDelegate didDismissGiraffeInterstitialViewController:payload];
        }
    }];
}


- (id)initWithDocument:(NSString *)document
{
    NSString* cachesDirectory = NSSearchPathForDirectoriesInDomains(NSCachesDirectory , NSUserDomainMask, YES)[0];
    NSString* fileLocation = [cachesDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/index.html",document]];
    
    documentPath = fileLocation;
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [webView setBackgroundColor:[UIColor blackColor]];
    webView.delegate = self;
    [webView setOpaque:NO];
    webView.alpha = 0;
    webView.scrollView.scrollEnabled = NO;
    webView.scrollView.bounces = NO;
    webView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:webView];
    [webView setAutoresizesSubviews:YES];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:documentPath isDirectory:NO]]];
    
    return [super init];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.view setBackgroundColor:[UIColor clearColor]];
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)shouldAutorotate
{
    return NO;
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark utils
-(NSString *)parsePayloadToJSON:(NSDictionary *)dictionary
{
    if(dictionary)
    {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self.payload options:NSJSONWritingPrettyPrinted error:&error];
        if (!jsonData)
        {
            NSLog(@"Unable to parse JSON: %@", error);
            return @"{}";
        }
        else
        {
            return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];;
        }
    }
    else
    {
        return @"{}";
    }
}

@end
