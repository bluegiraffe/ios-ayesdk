//
//  GiraffeInterstitialWebViewController.h
//  GiraffeSDK
//
//  Created by Bart van den Berg on 25/01/2017.
//  Copyright © 2017 BlueGiraffe Games B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GiraffeInterstitialViewControllerDelegate;
@interface GiraffeInterstitialViewController : UIViewController<UIWebViewDelegate>
{
    UIWebView *webView;
    NSString *documentPath;
}

@property (nonatomic, weak) id<GiraffeInterstitialViewControllerDelegate> _Nullable delegate;
@property (strong, nonatomic) NSDictionary * _Nullable payload;

- (id _Nonnull)initWithDocument:(NSString *_Nonnull)document;


@end

@protocol GiraffeInterstitialViewControllerDelegate <NSObject>
-(void)didDismissGiraffeInterstitialViewController:(NSMutableDictionary *_Nullable)payload;
@end

