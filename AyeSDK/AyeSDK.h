//
//  AyeSDK.h
//  AyeSDK
//
//  Created by Bart van den Berg on 10/16/17.
//  Version 0.0.1
//  Copyright © 2018 Blue Giraffe Games B.V. All rights reserved.
//
#pragma once
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#define kAyeSDKConnectVersionString @"0.0.1"

@protocol AyeSDKDelegate;
@interface AyeSDK : NSObject
@property (nonatomic) NSString *_Nullable globalId;
@property (nonatomic) NSString *_Nullable playerId;
@property (nonatomic) NSString *_Nullable installId;
@property (nonatomic) NSString *_Nullable consentId;
@property (nonatomic) NSString *_Nullable termsURL;

@property (nonatomic) long timestamp;
@property (nonatomic, weak) id<AyeSDKDelegate> _Nullable delegate;
@property (nonatomic, assign) Boolean connected;
@property (nonatomic, assign) Boolean debugMode;


+ (AyeSDK * _Nullable)manager;
+ (NSString *_Nullable)sdKVersion;

-(void)setInstallId:(NSString *_Nonnull)value;
-(void)setGlobalId:(NSString *_Nonnull)value;
-(void)setPlayerId:(NSString *_Nonnull)value;
-(void)setConsentId:(NSString *_Nonnull)value;


-(void)checkPrivacyWithCompletion:(void (^ _Nullable)(NSDictionary * _Nullable install))onCompletion onError:(void (^_Nullable)(NSError * _Nullable error))onError;


/*!
 @brief Upload a file to the backend
 @param filePath with the relative path
 @discussion return Name of the file and Url to download it from
 */
-(void)uploadPlayerFile:(NSString* _Nonnull)filePath andCompletion:(void (^_Nullable)(NSMutableDictionary * _Nonnull result, NSHTTPURLResponse *_Nonnull response))onCompletion onError:(void (^_Nullable)(NSError * _Nullable error, NSHTTPURLResponse *_Nullable response))onError;

/*!
 @brief Download a file to the backend
 @param playerId of the specific playerfile
 @discussion return location of the file that is stored
 */
-(void)downloadPlayerFile:(NSString *_Nonnull)playerId andCompletion:(void (^_Nonnull)(NSString * _Nonnull location))onCompletion onError:(void (^_Nonnull)(NSError * _Nullable error, NSHTTPURLResponse *_Nullable response))onError;


@end

#pragma mark delegate methods
@protocol AyeSDKDelegate <NSObject>
-(void)didReceiveLockingMessage:(NSDictionary *_Nullable)payload withLockState:(Boolean)lock withError:(NSError *_Nullable)error;
@end
