//
//  GiraffeReachability.m
//  GiraffeSDK
//
//  Created by Bart van den Berg on 20/09/16.
//  Copyright © 2016 BlueGiraffe Games B.V. All rights reserved.
//

#import "GiraffeReachability.h"

#import <sys/socket.h>
#import <netinet/in.h>
#import <netinet6/in6.h>
#import <arpa/inet.h>
#import <ifaddrs.h>
#import <netdb.h>

#import <CoreFoundation/CoreFoundation.h>
#import <SystemConfiguration/SystemConfiguration.h>

static GiraffeReachability * _sharedInstance = nil;

@interface GiraffeReachability ()
{
    SCNetworkReachabilityRef _reachabilityRef;
}
@end

@implementation GiraffeReachability
+ (GiraffeReachability *)reachabilityWithAddress:(const struct sockaddr_in *)hostAddress;
{
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault,
                                                                                   (const struct sockaddr*)hostAddress);
    GiraffeReachability* retVal = NULL;
    if(reachability != NULL)
    {
        retVal = [[self alloc] init];
        if(retVal != NULL)
        {
            retVal->_reachabilityRef = reachability;
        }
        else
        {
            CFRelease(reachability);
        }
    }
    return retVal;
}

+ (GiraffeReachability *)reachabilityForInternetConnection;
{
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    return [self reachabilityWithAddress: &zeroAddress];
}

#pragma mark -
#pragma mark Notifier

static void ReachabilityCallback(SCNetworkReachabilityRef target, SCNetworkReachabilityFlags flags, void* info)
{
    [[NSNotificationCenter defaultCenter] postNotificationName:PTReachabilityDidChangeNotification object:nil];
}

+ (BOOL)startGeneratingNotifications
{
    return [[self sharedInstance] scheduleNotifications];
}

+ (void)stopGeneratingNotificatons
{
    [[self sharedInstance] unscheduleNotificatons];
}

- (BOOL)scheduleNotifications
{
    BOOL retVal = NO;
    SCNetworkReachabilityContext context = {0, NULL, NULL, NULL, NULL};
    if(SCNetworkReachabilitySetCallback(_reachabilityRef, ReachabilityCallback, &context))
    {
        if(SCNetworkReachabilityScheduleWithRunLoop(_reachabilityRef, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode))
        {
            retVal = YES;
            //NSLog(@"PTReachability :: Reachability started generating notifications...");
            //CPLogDebug(CPTagCommon, @"Reachability started generating notifications");
        }
    }
    return retVal;
}

- (void)unscheduleNotificatons
{
    if(_reachabilityRef != NULL)
    {
        SCNetworkReachabilityUnscheduleFromRunLoop(_reachabilityRef, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
        //CPLogDebug(CPTagCommon, @"Reachability stopped generating notifications");
        //NSLog(@"PTReachability :: Reachability stopped generating notifications...");
    }
}

#pragma mark -
#pragma mark Network Flag Handling

- (GiraffeNetworkStatus)networkStatusForFlags:(SCNetworkReachabilityFlags)flags
{
    if ((flags & kSCNetworkReachabilityFlagsReachable) == 0)
    {
        // if target host is not reachable
        return PTNetworkStatusUnreachable;
    }
    
    GiraffeNetworkStatus retVal = PTNetworkStatusUnreachable;
    
    if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
    {
        // if target host is reachable and no connection is required
        //  then we'll assume (for now) that your on Wi-Fi
        retVal = PTNetworkStatusReachableWiFi;
    }
    
    
    if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
         (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
    {
        // ... and the connection is on-demand (or on-traffic) if the
        //     calling application is using the CFSocketStream or higher APIs
        
        if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
        {
            // ... and no [user] intervention is needed
            retVal = PTNetworkStatusReachableWiFi;
        }
    }
    
    if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
    {
        // ... but WWAN connections are OK if the calling application
        //     is using the CFNetwork (CFSocketStream?) APIs.
        retVal = PTNetworkStatusReachableCarrier;
    }
    return retVal;
}

- (GiraffeNetworkStatus)resolveReachabilityStatus
{
    //CPAssert(_reachabilityRef != NULL);
    
    SCNetworkReachabilityFlags flags;
    if (SCNetworkReachabilityGetFlags(_reachabilityRef, &flags))
    {
        return [self networkStatusForFlags:flags];
    }
    return PTNetworkStatusReachableUnknown;
}

+ (GiraffeNetworkStatus)currentReachabilityStatus
{
    return [[self sharedInstance] resolveReachabilityStatus];
}

+ (NSString *)currentReachabilityStatusString
{
    GiraffeNetworkStatus status = [self currentReachabilityStatus];
    switch (status) {
        case PTNetworkStatusReachableWiFi:
            return PTNetworkReachableWiFi;
        case PTNetworkStatusReachableCarrier:
            return PTNetworkReachableCarrier;
        case PTNetworkStatusUnreachable:
            return PTNetworkUnreachable;
        case PTNetworkStatusReachableUnknown:
            return PTNetworkReachableUnknown;
    }
    
    return PTNetworkReachableUnknown;
}

+ (GiraffeReachability *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [GiraffeReachability reachabilityForInternetConnection];
    });
    
    return _sharedInstance;
}
@end
