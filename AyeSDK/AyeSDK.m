//
//  AyeSDK.m
//  AyeSDK
//
//  Created by Bart van den Berg on 08/09/16.
//  Copyright © 2016 BlueGiraffe Games B.V. All rights reserved.
//

#import "AyeSDK.h"
#import "GiraffeReachability.h"
#include <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonKeyDerivation.h>
#import <CommonCrypto/CommonCryptor.h>
#import "NSDictionary+BearQueryStringBuilder.h"
#import "NYAlertViewController.h"

@import MobileCoreServices;


//#ifdef _STAGING
//#warning You're running a staging version of the AyeSDK
// Staging connection
NSString *const baseApiURL = @"https://api.test.gamehouseoriginalstories.com/liveopshm4";
//#endif // _STAGING

#ifdef _LIVE
// Live connection
NSString *const baseApiURL = @"https://api.test.gamehouseoriginalstories.com/liveopshm4";
#endif // _LIVE

// Install
NSString *const endPointInstallAdd = @"/install";

// Lock
NSString *const endPointLock = @"/lock";

// Upload
NSString *const endPointUpload = @"/playerState";
NSString *const endPointDownload = @"/playerState";

// Player
NSString *const endPointPlayerGet = @"/player";
NSString *const endPointPlayerAdd = @"/player";

// Diamonds
NSString *const endPointDiamonds = @"/inventory/diamonds";
NSString *const endPointDiamondsAdd = @"/inventory/diamonds/add";
NSString *const endPointDiamondsConsume = @"/inventory/diamonds/consume";

// Tickets
NSString *const endPointTickets = @"/inventory/tickets";
NSString *const endPointTicketsAdd = @"/inventory/tickets/add";
NSString *const endPointTicketsConsume = @"/inventory/tickets/consume";

// Hearts
NSString *const endPointHearts = @"/inventory/hearts";
NSString *const endPointHeartsAdd = @"/inventory/hearts/add";
NSString *const endPointHeartsConsume = @"/inventory/hearts/consume";


@implementation AyeSDK
{
    NSURLSession *httpRequestSession;
    NSString *filePathDataStoreUserConfiguration;
    NSMutableArray *queueHttpPosts;
    Boolean isSendingHttpRequest;
    Boolean didReceivePushnotificationToken;
    Boolean isLockCheckingEnabled;
    Boolean isLockCheckReady;
    NSTimer *lockTimer;
    NSUserDefaults *defaults;
    
}
@synthesize debugMode;
@synthesize globalId;
@synthesize installId;
@synthesize playerId;

@synthesize consentId;

@synthesize timestamp;

+ (AyeSDK *)manager
{
    static AyeSDK *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[AyeSDK alloc] init];
        
    });
    return manager;
}

-(id)init
{
    if ((self = [super init]))
    {
        // setup directory with stored objects
        NSString* libraryDirectory = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory , NSUserDomainMask, YES)[0];
        filePathDataStoreUserConfiguration = [libraryDirectory stringByAppendingPathComponent:@"AyeSDKConfiguration.plist"];
        [self loadConfiguration];
        queueHttpPosts = [NSMutableArray array];
        self.connected = false;
        
        defaults = [NSUserDefaults standardUserDefaults];
        
        
        // Set up the default session handler
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        configuration.HTTPAdditionalHeaders = @{@"apiKey": @""};
        httpRequestSession = [NSURLSession sessionWithConfiguration:configuration];
        
        NSLog(@"AyeSDK :: Version %@ initialized", [AyeSDK sdKVersion]);
        
        // check for reachability changes
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange) name:PTReachabilityDidChangeNotification object:nil];
        [GiraffeReachability startGeneratingNotifications];
        [self checkReachability];

    }
    return self;

}

#pragma mark configuration methods
-(void)loadConfiguration
{
    globalId = [defaults objectForKey:@"globalId"];
    installId = [defaults objectForKey:@"installId"];
    consentId = [defaults objectForKey:@"consentId"];
    playerId = [defaults objectForKey:@"playerId"];
}

-(void)saveConfiguration
{
    [defaults setObject:globalId forKey:@"globalId"];
    [defaults setObject:installId forKey:@"installId"];
    [defaults setObject:consentId forKey:@"consentId"];
    [defaults setObject:playerId forKey:@"playerId"];
    [defaults synchronize];
}



#pragma mark setters customized for storing
-(void)setInstallId:(NSString *_Nonnull)value
{
    installId = value;
    [self saveConfiguration];
}

-(void)setGlobalId:(NSString *_Nonnull)value
{
    globalId = value;
    [self saveConfiguration];
}

-(void)setPlayerId:(NSString *_Nonnull)value
{
    playerId = value;
    [self saveConfiguration];
}

-(void)setConsentId:(NSString *_Nonnull)value
{
    consentId = value;
    [self saveConfiguration];
}

#pragma mark notification methods
-(void)reachabilityDidChange
{
    [self checkReachability];
}

-(void)checkReachability
{
    if([GiraffeReachability currentReachabilityStatus] == PTNetworkStatusUnreachable)
    {
        // No connection
        self.connected = NO;
        [self BearLog:@"No connection..."];
    }
    else
    {
        // Connected
        self.connected = YES;
        [self BearLog:@"AyeSDK has network connection..."];
    }
}

#pragma mark model methods
-(void)checkPrivacyWithCompletion:(void (^)(NSDictionary *install))onCompletion onError:(void (^)(NSError * _Nullable error))onError;
{
    [self loadConfiguration];
    if (consentId.length>0 && _termsURL.length>0)
    {
        onCompletion(@{});
    }
    else
    {
        if (self.connected)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
                
                // Set a title and message
                alertViewController.showsStatusBar = NO;
                alertViewController.title = NSLocalizedString(@"Terms and Conditions", nil);
                alertViewController.message = NSLocalizedString(@"We want you to know exactly how our game works and what data we collect. Please state that you have read and agreed to these terms before you continue.", nil);
                
                // Customize appearance as desired
                alertViewController.buttonCornerRadius = 20.0f;
                
                alertViewController.titleFont = [UIFont fontWithName:@"AvenirNext-Bold" size:18.0f];
                alertViewController.messageFont = [UIFont fontWithName:@"AvenirNext-Medium" size:14.0f];
                alertViewController.buttonTitleFont = [UIFont fontWithName:@"AvenirNext-Regular" size:alertViewController.buttonTitleFont.pointSize];
                alertViewController.cancelButtonTitleFont = [UIFont fontWithName:@"AvenirNext-Medium" size:alertViewController.cancelButtonTitleFont.pointSize];
                
                alertViewController.swipeDismissalGestureEnabled = NO;
                alertViewController.backgroundTapDismissalGestureEnabled = NO;

                [alertViewController addAction:[NYAlertAction actionWithTitle:NSLocalizedString(@"Read more", nil) style:UIAlertActionStyleDefault handler:^(NYAlertAction *action)
                                                {
                                                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_termsURL]];
                                                }]];
                
                [alertViewController addAction:[NYAlertAction actionWithTitle:NSLocalizedString(@"Continue", nil) style:UIAlertActionStyleCancel handler:^(NYAlertAction *action)
                                                {
                                                    //                [self setConsentId:@"yes"];
                                                    onCompletion(@{});
                                                    [alertViewController dismissViewControllerAnimated:YES completion:nil];
                                                }]];
                
                
                [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:^{
                    
                }];
                
                
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
                
                // Set a title and message
                alertViewController.showsStatusBar = NO;
                alertViewController.title = NSLocalizedString(@"Unable to connect", nil);
                alertViewController.message = NSLocalizedString(@"Make sure you are connected to the internet before you continue.", nil);
                
                // Customize appearance as desired
                alertViewController.buttonCornerRadius = 20.0f;
                
                alertViewController.titleFont = [UIFont fontWithName:@"AvenirNext-Bold" size:18.0f];
                alertViewController.messageFont = [UIFont fontWithName:@"AvenirNext-Medium" size:14.0f];
                alertViewController.buttonTitleFont = [UIFont fontWithName:@"AvenirNext-Regular" size:alertViewController.buttonTitleFont.pointSize];
                alertViewController.cancelButtonTitleFont = [UIFont fontWithName:@"AvenirNext-Medium" size:alertViewController.cancelButtonTitleFont.pointSize];
                
                alertViewController.swipeDismissalGestureEnabled = NO;
                alertViewController.backgroundTapDismissalGestureEnabled = NO;
                
                [alertViewController addAction:[NYAlertAction actionWithTitle:NSLocalizedString(@"Continue", nil) style:UIAlertActionStyleCancel handler:^(NYAlertAction *action)
                                                {
                                                    [self checkPrivacyWithCompletion:^(NSDictionary * _Nullable install) {
                                                        onCompletion(install);
                                                    } onError:^(NSError * _Nullable error) {
                                                        onError(error);
                                                    }];
                                                    
                                                    [alertViewController dismissViewControllerAnimated:YES completion:nil];
                                                }]];
                
                
                [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:^{
                    
                }];
                
                
            });
        }
        
    }
}



-(void)addInstallIdWithDeviceName:(NSString *)deviceName WithCompletion:(void (^)(NSDictionary *install))onCompletion onError:(void (^)(NSError * _Nullable error))onError;
{
    if(globalId && deviceName && deviceName.length>0)
    {
        NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
        [postData setValue:self.globalId forKey:@"globalId"];
        [postData setValue:deviceName forKey:@"deviceName"];
        
        [self postDictionaryTo:endPointInstallAdd withPostdata:postData andCompletion:^(NSMutableDictionary * _Nonnull result, NSHTTPURLResponse * _Nonnull response)
        {
            if(result)
            {
                if((long)[response statusCode] == 200)
                {
                    NSDictionary *data = [result objectForKey:@"data"];
                    if(data.count>0)
                    {
                        onCompletion(data);
                    }
                    else
                    {
                        onCompletion(@{});
                    }
                }
                else
                {
                    onCompletion(@{});
                }
            }
            else
            {
                onCompletion(@{});
            }
        } onError:^(NSError * _Nullable error, NSHTTPURLResponse * _Nullable response) {
            onError(error);
        }];
    }
    else
    {
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"Unable to do request", nil),
                                   NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Missing globalId", nil),
                                   NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Make sure you set globalId before you do this request", nil)};
        NSError *error = [NSError errorWithDomain:@"BearGlobalIdError" code:-57 userInfo:userInfo];
        onError(error);
    }
}




-(void)uploadPlayerFile:(NSString*)filePath andCompletion:(void (^)(NSMutableDictionary * _Nonnull result, NSHTTPURLResponse *_Nonnull response))onCompletion onError:(void (^)(NSError * _Nullable error, NSHTTPURLResponse *_Nullable response))onError;
{
    NSMutableDictionary *data = [[NSMutableDictionary alloc]init];
    [self uploadFileTo:endPointUpload withPostdata:data File:filePath andCompletion:^(NSMutableDictionary * _Nonnull result, NSHTTPURLResponse * _Nonnull response)
    {
        if(result)
        {
            onCompletion(result,response);
        }
        else
        {
            onError(nil,response);
        }
        
    }
        onError:^(NSError * _Nullable error, NSHTTPURLResponse * _Nullable response)
    {
        onError(error,response);
    }];
}

-(void)downloadPlayerFile:(NSString *)playerId andCompletion:(void (^)(NSString * _Nonnull location))onCompletion onError:(void (^)(NSError * _Nullable error, NSHTTPURLResponse *_Nullable response))onError;
{
    //Get file download location
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    [self downloadFileFrom:endPointDownload forPlayerId:playerId storeFile:[NSString stringWithFormat:@"%@/%@.dat",documentsDirectory,playerId] andCompletion:^(NSString * _Nonnull location) {
        onCompletion(location);
    } onError:^(NSError * _Nullable error, NSHTTPURLResponse * _Nullable response) {
        onError(error,response);
    }];
}



#pragma mark internal methods
-(void)getDictionaryFrom:(NSString *)endPointUrl withQueryParams:(NSDictionary *)data andCompletion:(void (^)(NSMutableDictionary * _Nonnull result, NSHTTPURLResponse *_Nonnull response))onCompletion onError:(void (^)(NSError * _Nullable error, NSHTTPURLResponse *_Nullable response))onError;
{
    NSMutableURLRequest *requestHttpGet;
    NSString *requestUrlString = [NSString stringWithFormat:@"%@%@",baseApiURL,endPointUrl];
    
    if(data)
    {
        requestUrlString = [self addQueryStringToUrlString:requestUrlString withDictionary:data];
    }
    
    requestHttpGet = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrlString] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
    
    [requestHttpGet setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestHttpGet setHTTPMethod:@"GET"];
    
    [self BearLog:[NSString stringWithFormat:@"GET %@ with params: %@",[NSString stringWithFormat:@"%@%@",baseApiURL,endPointUrl], data]];
        
        NSURLSessionDataTask * httpTask = [httpRequestSession dataTaskWithRequest:requestHttpGet completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
           {
               NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
               if(!error)
               {
                   
                   NSError *jsonError;
                   NSMutableDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                   NSString *rawData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                   [self BearLog:[NSString stringWithFormat:@"GET :: Receiving raw response: %@",rawData]];
                   
                   if(jsonError)
                   {
                       [self BearLog:[NSString stringWithFormat:@"GET :: Error parsing JSON: %@",[jsonError description]]];
                       onError(jsonError, httpResponse);
                   }
                   else
                   {
                       [self BearLog:[NSString stringWithFormat:@"GET :: Received response: %@ \nfrom response: %@",[error description],[httpResponse description]]];
                       onCompletion(jsonResponse, httpResponse);
                   }
               }
               else
               {
                   onError(error,httpResponse);
               }
           }];
        
        [httpTask resume];
    
}


-(void)postDictionaryTo:(NSString *)endPointUrl withPostdata:(NSMutableDictionary *)data andCompletion:(void (^)(NSMutableDictionary * _Nonnull result, NSHTTPURLResponse *_Nonnull response))onCompletion onError:(void (^)(NSError * _Nullable error, NSHTTPURLResponse *_Nullable response))onError;
{
    NSMutableURLRequest *requestHttpPost = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseApiURL,endPointUrl]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
    
    [requestHttpPost setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestHttpPost setHTTPMethod:@"POST"];
    
    if(data == nil)
    {
        data = [[NSMutableDictionary alloc]init];
    }
    
    if(self.installId)
    {
        [data setValue:self.installId forKey:@"installId"];
    }
    
    if(self.playerId)
    {
       [data setValue:self.playerId forKey:@"playerId"];
    }
    
    
    [self BearLog:[NSString stringWithFormat:@"POST %@ with body: %@",[NSString stringWithFormat:@"%@%@",baseApiURL,endPointUrl], data]];
    
    NSError *jsonError;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&jsonError];
    
    if(!jsonError)
    {
        [requestHttpPost setHTTPBody:postData];
        
        NSURLSessionDataTask * httpTask = [httpRequestSession dataTaskWithRequest:requestHttpPost completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
           {
               NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
               if(!error)
               {
                   
                   NSError *jsonError;
                   NSMutableDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                   NSString *rawData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                   [self BearLog:[NSString stringWithFormat:@"POST :: Receiving raw response: %@",rawData]];
                   
                   if(jsonError)
                   {
                       [self BearLog:[NSString stringWithFormat:@"POST :: Error parsing JSON: %@",[jsonError description]]];
                       onError(jsonError, httpResponse);
                   }
                   else
                   {
                       [self BearLog:[NSString stringWithFormat:@"POST :: Received response: %@ \nfrom request: %@",[jsonResponse description], [httpResponse description]]];
                       onCompletion(jsonResponse, httpResponse);
                   }
               }
               else
               {
                   [self BearLog:[NSString stringWithFormat:@"POST :: Error getting request: %@ \nfrom response: %@",[error description],[httpResponse description]]];
                   onError(error,httpResponse);
               }
           }];
        [httpTask resume];
    }
}

-(void)downloadFileFrom:(NSString *)endPointUrl forPlayerId:(NSString *)playerId storeFile:(NSString *)file andCompletion:(void (^)(NSString * _Nonnull location))onCompletion onError:(void (^)(NSError * _Nullable error, NSHTTPURLResponse *_Nullable response))onError;
{
    if(endPointUrl && playerId && file)
    {
        NSMutableURLRequest *requestHttpPost = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",baseApiURL,endPointUrl,playerId]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
        
        NSURLSessionDataTask * httpDownloadTask = [httpRequestSession dataTaskWithRequest:requestHttpPost completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                                           {
                                               NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                               if(!error && data)
                                               {
                                                   NSString *fileName = [NSString stringWithFormat:@"%@", file];
                                                   [data writeToFile:fileName atomically:YES];
                                                   onCompletion(fileName);
                                               }
                                               else
                                               {
                                                   [self BearLog:[NSString stringWithFormat:@"DOWNLOAD :: Error getting request: %@ \nfrom response: %@",[error description],[httpResponse description]]];
                                                   onError(error,httpResponse);
                                               }
                                           }];
        [httpDownloadTask resume];
    }
    else
    {
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"Unable to download file", nil),
                                   NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Missing endpointUrl, playerId or location", nil),
                                   NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Make sure all 3 the items are filled in to request the file", nil)};
        NSError *error = [NSError errorWithDomain:@"BearCurrencyAddError" code:-57 userInfo:userInfo];
        onError(error,nil);
    }
}

-(void)uploadFileTo:(NSString *)endPointUrl withPostdata:(NSMutableDictionary *)data File:(NSString *)fileUrl andCompletion:(void (^)(NSMutableDictionary * _Nonnull result, NSHTTPURLResponse *_Nonnull response))onCompletion onError:(void (^)(NSError * _Nullable error, NSHTTPURLResponse *_Nullable response))onError;
{
    NSMutableURLRequest *fileRequestHttpPost = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@?playerId=%@",baseApiURL,endPointUrl,self.playerId]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
    
    NSString *boundary = [self generateBoundaryString];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:data paths:@[fileUrl] fieldName:@"file"];
    
    [fileRequestHttpPost setValue:contentType forHTTPHeaderField: @"Content-Type"];
    [fileRequestHttpPost setHTTPMethod:@"POST"];
    
    [self BearLog:[NSString stringWithFormat:@"UPLOAD %@ with body: %@",[NSString stringWithFormat:@"%@%@",baseApiURL,endPointUrl], data]];
    
    NSURLSessionUploadTask *uploadTask = [httpRequestSession uploadTaskWithRequest:fileRequestHttpPost fromData:httpBody completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
    {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if(!error)
        {
            
            NSError *jsonError;
            NSMutableDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            NSString *rawData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            [self BearLog:[NSString stringWithFormat:@"UPLOAD :: Receiving raw response: %@",rawData]];
            
            if(jsonError)
            {
                [self BearLog:[NSString stringWithFormat:@"UPLOAD :: Error parsing JSON: %@",[jsonError description]]];
                onError(jsonError, httpResponse);
            }
            else
            {
                [self BearLog:[NSString stringWithFormat:@"UPLOAD :: Received response: %@ \nfrom request: %@",[jsonResponse description], [httpResponse description]]];
                onCompletion(jsonResponse, httpResponse);
            }
        }
        else
        {
            [self BearLog:[NSString stringWithFormat:@"UPLOAD :: Error getting request: %@ \nfrom response: %@",[error description],[httpResponse description]]];
            onError(error,httpResponse);
        }
    }];
    [uploadTask resume];
}


#pragma mark utils

-(NSString*)urlEscapeString:(NSString *)unencodedString
{
    CFStringRef originalStringRef = (__bridge_retained CFStringRef)unencodedString;
    NSString *s = (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,originalStringRef, NULL, (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ", kCFStringEncodingUTF8);
    CFRelease(originalStringRef);
    return s;
}

-(NSString*)addQueryStringToUrlString:(NSString *)urlString withDictionary:(NSDictionary *)dictionary
{
    NSMutableString *urlWithQuerystring = [[NSMutableString alloc] initWithString:urlString];
    
    for (id key in dictionary) {
        NSString *keyString = [key description];
        NSString *valueString = [[dictionary objectForKey:key] description];
        
        if ([urlWithQuerystring rangeOfString:@"?"].location == NSNotFound) {
            [urlWithQuerystring appendFormat:@"?%@=%@", [self urlEscapeString:keyString], [self urlEscapeString:valueString]];
        } else {
            [urlWithQuerystring appendFormat:@"&%@=%@", [self urlEscapeString:keyString], [self urlEscapeString:valueString]];
        }
    }
    return urlWithQuerystring;
}
-(NSString *)decryptMessage:(NSString *)payload andKey:(NSString *)iv64
{
    
    NSData *iv = [[NSData alloc] initWithBase64EncodedString:iv64 options:0];
    
    // Generate key from password and salt using SHA256 to hash and PDKDF2 to harden
    NSString* password = @"1234567890123456";
    NSString* salt = @"bluegiraffeisthebeststudiointhemotherfuckingworld!";
    NSMutableData* hash = [NSMutableData dataWithLength:CC_SHA256_DIGEST_LENGTH];
    NSMutableData* key = [NSMutableData dataWithLength:CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(salt.UTF8String, (CC_LONG)strlen(salt.UTF8String), hash.mutableBytes);
    CCKeyDerivationPBKDF(kCCPBKDF2, password.UTF8String, strlen(password.UTF8String), hash.bytes, hash.length, kCCPRFHmacAlgSHA1, 1000, key.mutableBytes, key.length);
    
    // NSLog(@"Hash : %@",[hash base64EncodedStringWithOptions:0]);
    // NSLog(@"Key : %@",[key base64EncodedStringWithOptions:0]);
    
    NSData* encryptedWithout64 = [[NSData alloc] initWithBase64EncodedString:payload options:0];
    NSMutableData* decrypted = [NSMutableData dataWithLength:encryptedWithout64.length + kCCBlockSizeAES128];
    size_t bytesDecrypted = 0;
    CCCrypt(kCCDecrypt,
            kCCAlgorithmAES128,
            kCCOptionPKCS7Padding,
            key.bytes,
            key.length,
            iv.bytes,
            encryptedWithout64.bytes, encryptedWithout64.length,
            decrypted.mutableBytes, decrypted.length, &bytesDecrypted);
    NSData* outputMessage = [NSMutableData dataWithBytes:decrypted.mutableBytes length:bytesDecrypted];
    NSString* outputString = [[NSString alloc] initWithData:outputMessage encoding:NSUTF8StringEncoding];
    
    return outputString;
}


-(NSString *)randomStringWithLength:(int)customLength
{
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: customLength];
    
    for (int i=0; i<customLength; i++)
    {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
    }
    
    return randomString;
}

-(BOOL)StringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (NSData *)createBodyWithBoundary:(NSString *)boundary
                        parameters:(NSDictionary *)parameters
                             paths:(NSArray *)paths
                         fieldName:(NSString *)fieldName {
    NSMutableData *httpBody = [NSMutableData data];
    
    // add params (all params are strings)
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    // add image data
    
    for (NSString *path in paths) {
        NSString *filename  = [path lastPathComponent];
        NSData   *data      = [NSData dataWithContentsOfFile:path];
        NSString *mimetype  = [self mimeTypeForPath:path];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldName, filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}

- (NSString *)mimeTypeForPath:(NSString *)path {
    // get a mime type for an extension using MobileCoreServices.framework
    
    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    assert(UTI != NULL);
    
    NSString *mimetype = @"application/octet-stream";//CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    
    assert(mimetype != NULL);
    
    CFRelease(UTI);
    
    return mimetype;
}

- (NSString *)generateBoundaryString {
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}

+ (NSString *_Nullable)sdKVersion;
{
    return kAyeSDKConnectVersionString;
}

-(void)BearLog:(NSString *)log
{
    if(debugMode)
    {
        NSLog(@"AyeSDK: %@",log);
    }
}

@end
